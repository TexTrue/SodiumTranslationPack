# Sodium Translation Pack

[EN](https://gitee.com/TexTrue/SodiumTranslationPack/blob/master/README-EN.md) / [ZH-CN](https://gitee.com/TexTrue/SodiumTranslationPack/blob/master/README.md)
## 这是什么？

在 [CaffeineMC/sodium-fabric](https://github.com/CaffeineMC/sodium-fabric) Pull [#100](https://github.com/CaffeineMC/sodium-fabric/pull/100)、[#517](https://github.com/CaffeineMC/sodium-fabric/pull/517)、[#717](https://github.com/CaffeineMC/sodium-fabric/pull/717)对本地化进行了支持并被整合到[CaffeineMC/sodium-fabric](https://github.com/CaffeineMC/sodium-fabric)/[1.17.x/dev](https://github.com/CaffeineMC/sodium-fabric/tree/1.17.x/dev) 分支,但作者未将[OneSky](https://jellysquid.oneskyapp.com/collaboration/project?id=366422)上的翻译移动到模组内。

### 此材质包分为多个版本，详见下表：

|          材质包名称           |    模组支持     | 游戏版本支持  | ModLoader |
| :---------------------------: | :-------------: | :-----------: | :-------: |
|     SodiumTranslationPack     |     Sodium      | 1.16.x/1.17.x |  Fabric   |
| SodiumReforgedTranslationPack | Sodium Reforged |    1.16.x     |   Forge   |
|  Sodium-ExtraTranslationPack  |  Sodium-Extra   |     未知①     |  Fabric   |

> ①：由于Sodium-Extra不支持本地化，所以暂不支持。

### 支持语言：

| SodiumTranslationPack | SodiumReforgedTranslationPack | Sodium-ExtraTranslationPack[暂不支持] |
| :-------------------: | :---------------------------: | :-----------------------------------: |
|  英语-美国（源语言）  |      英语-美国（源语言）      |          英语-美国（源语言）          |
|       简体中文        |           简体中文            |               简体中文                |
|                       |                               |                                       |



### 感谢

Sodium作者**JellySquid**

SodiumReforged作者**spoorn**

Sodium-Extra作者**FlashyReese**

**MojangStudio**
