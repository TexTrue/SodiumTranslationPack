# Sodium Translation Pack

[EN](https://gitee.com/TexTrue/SodiumTranslationPack/blob/master/README-EN.md) / [ZH-CN](https://gitee.com/TexTrue/SodiumTranslationPack/blob/master/README.md)
## What's this?

Localization is supported in [CaffeineMC/sodium-fabric](https://github.com/CaffeineMC/sodium-fabric) Pull [#100](https://github.com/CaffeineMC/sodium-fabric/pull/100) [#517](https://github.com/CaffeineMC/sodium-fabric/pull/517) [#717](https://github.com/CaffeineMC/sodium-fabric/pull/717) and integrated into [CaffeineMC/sodium-fabric](https://github.com/CaffeineMC/sodium-fabric)/[1.17.x/dev](https://github.com/CaffeineMC/sodium-fabric/tree/1.17.x/dev). However, the translation on [OneSky](https://jellysquid.oneskyapp.com/collaboration/project?id=366422) is not moved into the Sodium.

### This material package is divided into several versions, as shown in the table below:

|       ResourcePack Name       |   Mod Support   | Game Version Support | ModLoader |
| :---------------------------: | :-------------: | :------------------: | :-------: |
|     SodiumTranslationPack     |     Sodium      |    1.16.x/1.17.x     |  Fabric   |
| SodiumReforgedTranslationPack | Sodium Reforged |        1.16.x        |   Forge   |
|  Sodium-ExtraTranslationPack  |  Sodium-Extra   |       UNKNOWN①       |  Fabric   |

> ①：Since the medium extra does not support localization, it is not supported at the moment.

### Language support:

|     SodiumTranslationPack      | SodiumReforgedTranslationPack  | Sodium-ExtraTranslationPack [NotSupported] |
| :----------------------------: | :----------------------------: | :----------------------------------------: |
| English UnitedStates(Original) | English UnitedStates(Original) |       English UnitedStates(Original)       |
|       Simplified Chinese       |       Simplified Chinese       |             Simplified Chinese             |
|              NONE              |              NONE              |                    NONE                    |

